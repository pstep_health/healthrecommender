package com.pstep

data class Health (
    val is_able_to_walk_5_minutes: Int,
    val days_per_week: Int,
    val minutes_per_day: Int,
    val no_problem_walking: Int,
    val some_problem_walking: Int,
    val severe_problem_walking: Int,
)