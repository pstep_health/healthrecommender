package com.pstep

class HealthRecommender {

    /*
    * Returns number of minutes which is a
    * weekly goal, that should be covered in a week.
    *
    * @param  health  an object of type com.pstep.Health which is a DTO
    * @return         weekly goal in minutes
     */
    fun calculateBaselineGoal(health: Health): Int {

        var goal: Int
        goal = health.minutes_per_day*health.days_per_week

        if (health.is_able_to_walk_5_minutes == 0 || health.severe_problem_walking == 1)
            goal = 3*5
        else if (goal != 0 && health.severe_problem_walking == 1) {
            if (goal > 150 || goal+15 > 150)
                goal = 150
            else
                goal += 15
        } else if (goal != 0 && health.severe_problem_walking == 0) {
            if (goal > 150 || goal+35 > 150)
                goal = 150
            else
                goal += 35
        }

        return goal
    }

    /*
    * Returns number of minutes which is a
    * next week goal, that should be covered in a following week.
    *
    * @param  week  an object of type com.pstep.WeekStats which is a DTO
    * @return       next week goal in minutes
     */
    fun calculateNextWeekGoal(week: WeekStats): Int {

        val newGoal: Int

        if (week.previous_week_goal == 0)
            newGoal = 0
        else if (week.minutes_achieved_previous_week < week.previous_week_goal) {
            if (week.severe_problem_walking == 0 && week.reduce == 4)
                newGoal = week.first_week_goal + 15                            //Step down to 15 + first week's goal if the user hasn't achieved the goal for consecutive 2 cycles
            else {
                newGoal = week.previous_week_goal //haven't met previous goal
                week.reduce += 1
            }

        } else {
            if (week.severe_problem_walking == 1) {
                newGoal = if (week.week % 2 == 0)
                    week.previous_week_goal
                else {
                    if ((week.previous_week_goal + 15) >= 150)     //checking goal not to exceed more than 150
                        150
                    else
                        week.previous_week_goal + 15            //default addition of 15 minutes in previous week goal
                }
            } else {
                newGoal = if (week.week % 2 == 0)
                    week.previous_week_goal
                else {
                    if ((week.previous_week_goal + 35) >= 150)     //checking goal not to exceed more than 150
                        150
                    else
                        week.previous_week_goal + 35            //default addition of 35 minutes in previous week goal
                }
            }
        }
        return newGoal
    }
}