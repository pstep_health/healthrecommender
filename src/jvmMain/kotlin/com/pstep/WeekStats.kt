package com.pstep

data class WeekStats (
    val previous_week_goal: Int,
    val minutes_achieved_previous_week: Int,
    val week: Int,
    val severe_problem_walking: Int,
    var reduce: Int,
    val first_week_goal: Int
)